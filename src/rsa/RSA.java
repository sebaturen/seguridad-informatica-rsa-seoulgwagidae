package rsa;

import userControl.ClientMode;
import userControl.ServerMode;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Parent;
import panel.SwitchMode;


/**
 * Main class!!
 * @author Seba
 */
public class RSA extends Application {
	
	//Variable
	public static byte runMode = -1;
	public static String userName;
            
	@Override
	public void start(Stage primaryStage)
	{
		showSwitchMode();
		if(runMode != -1)
		{
			Parent runModePanel;
			String title = "Server";
			switch(runMode)
			{
				case ConnectInformation.CLIENT_MODE:
					ClientMode clientMode = new ClientMode(userName, runMode);
					runModePanel = clientMode.getPanel();
					title = "Client - "+ userName;
					break;
				default: //SERVER MODE
					ServerMode serverMode = new ServerMode(userName, runMode);
					runModePanel = serverMode.getPanel();
					serverMode.runServer();
					break;
			}
			
			Scene scene = new Scene(runModePanel, 460, 555);
			primaryStage.setTitle(title);
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.show();
		}
		
	}
	
	/**
	 * Switch Stage
	 */
	private void showSwitchMode()
	{
		Stage switchStage = new Stage();
		switchStage.setTitle("Mode Selector"); 
		switchStage.setResizable(false);
		Scene scene = new Scene(new SwitchMode(switchStage), 280, 200);
		switchStage.setScene(scene);
		switchStage.showAndWait();
	}
    
	/**
	 * The main method is only needed for the IDE with limited
	 * JavaFX support. Not needed for running from the command line.
	 */
	public static void main(String[] args) {
		launch(args);
	}
    
    
}
