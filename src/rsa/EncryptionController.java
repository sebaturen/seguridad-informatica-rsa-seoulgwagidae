package rsa;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import userControl.Mode;

/**
 * Cryptography controller.
 * RSA:
 * Generate a Private and public Key
 * AES:
 * Generate a Symmetric key
 * Signature:
 * Validate integrity with a file, use a RSA key to create a signature.
 * @author Seba
 */
public final class EncryptionController implements ConnectInformation {
    
    //CONSTANTE
    public static final String ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITH = "SHA512WithRSA";
    public static final String SYMMETRIC_ALGORITH = "AES";
    public static final String SYMMETRIC_ENCRYPT_ALGORITH = "AES/CBC/PKCS5Padding";
    public static final int KEY_SIZE = 1024;
    
    //Variable
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private PublicKey conectPublicKey; //Different private key with a connection, client or server public key
    private SecretKey symmetricKey;
    
    //Log String
    private static String logError = "";
    
    //Constructor
    public EncryptionController() 
    {
        try {
            //Try load my private and public key is previews save:
            loadKey();
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException ex) {
            try {
                generateKey();
                saveKey(); //Save new generate key
            } catch (NoSuchAlgorithmException | IOException ex1) {
                addLog("Fail to generate the KEY");
            }
        }
    }
    
    /* ============================================== */
    /*    Public - Private KEY Control  - section     */
    /* ============================================== */
    /**
     * Load the key from the file
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws InvalidKeySpecException
     */
    public void loadKey() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException
    {
        //Get a File location:
        String fileKeyLocation = Mode.getFileLocation();
        KeyFactory kf = KeyFactory.getInstance(ALGORITHM);
        
        //Load public key
        publicKey = kf.generatePublic(new X509EncodedKeySpec(readKeyFile(fileKeyLocation + PUBLIC_KEY_FILE)));
        privateKey = kf.generatePrivate(new PKCS8EncodedKeySpec(readKeyFile(fileKeyLocation + PRIVATE_KEY_FILE)));
        
        //Log
        addLog("The KEY is loaded from the file with SUCCESS");
    }
    
    /**
     * Load a paired public key
     * @param conectName
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws InvalidKeySpecException
     */
    public void loadPariedPubliKeyFile( String conectName ) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException
    {
        //Set location
        String fileKeyLocation = Mode.getFileLocation() + PARIED_PUBLY_KEY_PATH;
        KeyFactory kf = KeyFactory.getInstance(ALGORITHM);
        
        //Get a key information
        conectPublicKey = kf.generatePublic(new X509EncodedKeySpec(readKeyFile(fileKeyLocation + conectName + PREFIX_PARIED_PUBLY_KEY_FILE)));
        
        //Log
        addLog("Connected public KEY is LOAD: "+ conectName);
    }
    
    /**
     * Read key file information
     * @param filePath
     * @return Key from file~
     * @throws FileNotFoundException
     * @throws IOException
     */
    private byte[] readKeyFile(String filePath) throws FileNotFoundException, IOException
    {
    	//Generate internal file
        File file = new File(filePath);
        int length = (int) file.length();
        byte[] bytes;
        //Read key file
        try (BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file))) {
            bytes = new byte[length];
            reader.read(bytes, 0, length);
        }
        return bytes;        
    }
    
    /**
     * save in a KEY file the actual key (the file is save in binary)
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void saveKey() throws FileNotFoundException, IOException
    {
        if(!isKey())
            addLog("The KEY has not yet been created");
        else
        {
            String fileKeyLocation = Mode.getFileLocation();

            //Generate directory from file
            String keyDirectory = fileKeyLocation + PRIVATE_KEY_FILE;
            File keyPath = new File(keyDirectory.replaceAll("(\\w+)_(\\d+).*", "$1/$2/$0"));
            keyPath.getParentFile().mkdirs();

            //Public Key
            byte[] publicKeyData = publicKey.getEncoded();
            try (OutputStream stream = new FileOutputStream(fileKeyLocation + PUBLIC_KEY_FILE)) {
                stream.write(publicKeyData);
                stream.close();
            }

            //Private Key
            byte[] privateKeyData = privateKey.getEncoded();
            try (OutputStream stream = new FileOutputStream(fileKeyLocation + PRIVATE_KEY_FILE)) {
                stream.write(privateKeyData);
                stream.close();
            }
            
            //If have a public server - client key save.
            if(isConnectPubliKey())
            {
            	//Generate directory
                fileKeyLocation = Mode.getFileLocation() + PARIED_PUBLY_KEY_PATH;                
                keyDirectory = fileKeyLocation + PRIVATE_KEY_FILE;
                keyPath = new File(keyDirectory.replaceAll("(\\w+)_(\\d+).*", "$1/$2/$0"));
                keyPath.getParentFile().mkdirs();
                
                //Paired key save
                byte[] paredKeyData = conectPublicKey.getEncoded();
                try (OutputStream stream = new FileOutputStream(fileKeyLocation + Mode.getOpostName() + PREFIX_PARIED_PUBLY_KEY_FILE)) {
                    stream.write(paredKeyData);
                    stream.close();
                }
            }
            
            addLog("The KEY was SUCCESSFULLY SAVED in the file");
        }
    }
    
    /**
     * Generate a public and private key
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public void generateKey() throws NoSuchAlgorithmException, IOException
    {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORITHM);
        SecureRandom random = new SecureRandom();
        generator.initialize(KEY_SIZE, random);
        KeyPair keyPair = generator.generateKeyPair();
        publicKey = keyPair.getPublic();
        privateKey = keyPair.getPrivate();
        addLog("KEY SUCCESSFUL created");
    }
    
    /**
     * Save in class the paired key (Opposite client-server public key)
     * @param publicKeyContent
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws IOException 
     * @throws FileNotFoundException 
     */
    public void setConnectPublicKey(byte[] publicKeyContent) throws NoSuchAlgorithmException, InvalidKeySpecException, FileNotFoundException, IOException
    {        
        //Key Factory
        KeyFactory kf = KeyFactory.getInstance(ALGORITHM);
        //Read information
        conectPublicKey = kf.generatePublic(new X509EncodedKeySpec(publicKeyContent));
        addLog("Paired KEY is correct SAVE");
        saveKey(); //Save a new key shared in file
    }
    
    /**
     * Save a Symmetric key
     * @param symmetricKeyConecnt
     * @throws NoSuchAlgorithmException
     */
    public void setSymmetricKey(byte[] symmetricKeyConecnt) throws NoSuchAlgorithmException
    {
        symmetricKey = new SecretKeySpec(symmetricKeyConecnt, 0, symmetricKeyConecnt.length, SYMMETRIC_ALGORITH);
        addLog("Symmetric KEY is correct SAVE");
    }
    
    /**
     * Generate a Symmetric key
     * @throws NoSuchAlgorithmException
     */
    public void generateSymmetricKey() throws NoSuchAlgorithmException
    {
        KeyGenerator keyGen = KeyGenerator.getInstance(SYMMETRIC_ALGORITH);
        keyGen.init(128);
        symmetricKey = keyGen.generateKey();
        addLog("Symmetric KEY is generated SUCCESSFUL");
    }
        
    /* ============================================== */
    /*       Encryption/Description  - section        */
    /* ============================================== */  
    /**************************************************/
    /*******************RSA Method*********************/
    /**
     * Receive a String messenger and encrypted use the paired Public Key (RSA)
     * @param msg
     * @return Encrypted message
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public byte[] encriptMsg(String msg) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        return encriptMsg(msg.getBytes());
    }
    
    /**
     * From byte array generate a new encrypt byte array (RSA)
     * @param msgByte
     * @return Encrypted message
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public byte[] encriptMsg(byte[] msgByte) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, conectPublicKey);
        return cipher.doFinal(msgByte);        
    }
    
    /**
     * Get a encrypt data, and decrypt use a private key
     * @param msg
     * @return Decrypted data
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public byte[] desencriptMsg(byte[] msg) throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher.doFinal(msg);
    }

    /**************************************************/
    /*************Symmetric Method*********************/
    /**
     * Encrypt using the symmetric key
     * @param msg
     * @return Encrypt data
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidAlgorithmParameterException
     */
    public byte[] encriptSymmetricMsg(byte[] msg) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException
    {
        Cipher cp = Cipher.getInstance(SYMMETRIC_ENCRYPT_ALGORITH);
        byte[] iv = new byte[cp.getBlockSize()];
        
        IvParameterSpec ivParams = new IvParameterSpec(iv);
        cp.init(Cipher.ENCRYPT_MODE, symmetricKey,ivParams);
        
        return cp.doFinal(msg);
    }
    
    /**
     * Decrypy use a Symmetric key
     * @param msg
     * @return Decrypy data
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public byte[] desencriptSymmetricMsg(byte[] msg) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cp = Cipher.getInstance(SYMMETRIC_ENCRYPT_ALGORITH);
        byte[] ivByte = new byte[cp.getBlockSize()];
        
        IvParameterSpec ivParamsSpec = new IvParameterSpec(ivByte);
        cp.init(Cipher.DECRYPT_MODE, symmetricKey, ivParamsSpec);
        
        return cp.doFinal(msg);
    }

    /**************************************************/
    /*************Signature Method*********************/
    /**
     * Generate a signature from a byte array (Use a private key RSA method)
     * @param msg
     * @return Signing a file
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public byte[] createSignature(byte[] msg) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException
    {
        Signature sig = Signature.getInstance(SIGNATURE_ALGORITH);
        sig.initSign(privateKey);
        sig.update(msg);
        return sig.sign(); 
    }
    
    /**
     * Validate a signature from data. (Use a paired public key RSA method)
     * @param data
     * @param signature
     * @return Status signature
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    public boolean validateSignature(byte[] data, byte[] signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException
    {
        Signature sig = Signature.getInstance(SIGNATURE_ALGORITH);
        sig.initVerify(conectPublicKey);
        sig.update(data);
        return (sig.verify(signature));        
    }
    
    /* ============================================== */
    /*         Key Information  - section             */
    /* ============================================== */         
    /**
     * Informs if the key was generated (RSA)
     * @return is create o no public and private key
     */
    public boolean isKey()
    {
        if (publicKey != null)
            return (getPublicKey().length > 0);
    	System.out.println("leng: ");
        return false;
    }
    
    /**
     * Informs if we have a paired key (RSA)
     * @return is linked a paired key 
     */
    public boolean isConnectPubliKey()
    {
        return (conectPublicKey != null);
    }
   
    /**
     * Informs if we have a Symmetric key
     * @return is a generate symmetric key
     */
    public boolean isSymmetricKey()
    {
        return (symmetricKey != null);
    }
    
    /**
     * Return the public key
     * @return public key
     */
    public byte[] getPublicKey()
    {
        return publicKey.getEncoded();
    }
    
    /**
     * Return the symmetric key
     * @return symmetric key
     */
    public byte[] getSymmetricKey()
    {
        return symmetricKey.getEncoded();
    }
        
 
    /**
     * If you want show a paired key
     * @return String paired key
     */
    public String getStringConnectPubliKey()
    {
        return DatatypeConverter.printBase64Binary(conectPublicKey.getEncoded());  
    }
    
    /**
     * add a static log content
     * @param log
     */
    private void addLog(String log)
    {
        if (logError.length() > 0)
            logError += "\n";
        logError += log;
    }
    
    /**
     * Get a previous log
     * @return
     */
    public static String getLog()
    {
        String eLog = logError;
        logError = "";
        return eLog;
    }
    
    /**
     * Clear paired key
     */
    public void clearConectPublicKey()
    {
        conectPublicKey = null;
    }
    
}
