package rsa;

/**
 *	Generate constant in this project. 
 */
public interface ConnectInformation {

    //CONSTANTE
    //Mode type
    public static final byte CLIENT_MODE = 0;
    public static final byte SERVER_MODE = 1;
    //Message type
    public static final byte STRING_MSG_TRANSMISION = 0;
    public static final byte FILE_TRANSMISION = 1;
    public static final byte PUBLY_KEY_TRANSMISION = 2;
    public static final byte SYMETRIC_KEY_TRANSMISION = 3;
    public static final byte HI_INFORMATION_TRANSMISION = 4;
    public static final byte FAIL_DECRYPT_MSG_NOTIFICATION = 5;
    //File
    public static final String SERVER_FILE_PATH     	= "serverFile/";
    public static final String CLIENT_FILE_PATH     	= "clienteFile/";
    public static final String PARIED_PUBLY_KEY_PATH	= "pairedFile/";
    public static final String TRANSMITION_FILE_PATH	= "transmissionFile/";
    public static final String PUBLIC_KEY_FILE      	= "publicKeyFile.key";
    public static final String PRIVATE_KEY_FILE     	= "privateKeyFile.key";
    public static final String PREFIX_PARIED_PUBLY_KEY_FILE	= "_key.key";
        
}
