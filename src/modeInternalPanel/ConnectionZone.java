package modeInternalPanel;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import panel.AbsModePanel;

/**
 * Connection zone GUI:
 * _________________________________________________________
 * |														|
 * |	[			] [									]	|
 * |	[	1/4		] [									]	|
 * |	[	CLIENT/	] [		GENERAL LOG (TEXT AREA)		]	|
 * |	[	SERVER	] [									]	|
 * |	[			] [									]	|
 * |	Connection Status: (LABEL)							|
 * |________________________________________________________|	
 * @author sturen
 *
 */
public class ConnectionZone {
	
	private VBox connectZone;
	private TextArea logGeneralTextArea		= new TextArea();
	private Label statusConnection			= new Label("Connection Status: NOT CONNECTED");
	
	public ConnectionZone(VBox connectZone)
	{
		this.connectZone = connectZone;
		//Constant Design:
		logGeneralTextArea.setPrefRowCount(5);
		logGeneralTextArea.setEditable(false);
	}
	
	public VBox getConnectionZone(double parentWidth)
	{
		VBox connectionZone = new VBox(5);
		connectionZone.setPadding(new Insets(AbsModePanel.MARGEN_TOP, 
											AbsModePanel.MARGE_RIGHT, 
											0,
											AbsModePanel.MARGE_LEFT));
		
		//Zone1
		logGeneralTextArea.setPrefWidth( (parentWidth - (parentWidth /4)) - (AbsModePanel.MARGE_RIGHT + AbsModePanel.MARGE_LEFT));
		BorderPane zoneConnect = new BorderPane();
		zoneConnect.setLeft(connectZone);
		zoneConnect.setRight(logGeneralTextArea);
		
		connectionZone.getChildren().addAll(zoneConnect, statusConnection);
		
		return connectionZone;
	}
	
	/****************************************/
	/******* GUI Change LISTENER	*********/
	public void addLog(String s)
	{
		Platform.runLater(() -> { logGeneralTextArea.appendText(s +"\n"); });
	}
	
	public void setServerConect(String s)
	{
		Platform.runLater(() -> { statusConnection.setText(s); });
	}
	/******* (END) GUI Change LISTENER	*********/	
	/********************************************/

}
