package modeInternalPanel;

import java.io.File;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import panel.AbsModePanel;
import userControl.Mode;

/**
 * File zone GUI:
 * _____________________________________________________________________
 * |																	|
 * |	[	ATTACHE FILE RUT (TEXT AREA)	] { Attach File (BUTTON)}	|
 * |	{			Send File (BUTTON)								}	|
 * |____________________________________________________________________|	
 * @author sturen
 *
 */
public class FileZone {
	
	private TextField ruteFileTextField = new TextField();
	private Button attacheFileButton	= new Button("Attach File");
	private Button sendFileButton		= new Button("Send File");
	private File fileSelect;
	
	public FileZone(Mode modecontorl)
	{
		//Constant Design:
		enableToSendFile(false);
		sendFileButton.setDisable(true);
		ruteFileTextField.setEditable(false);
		ruteFileTextField.setDisable(true);
		
		//Action Listener
		sendFileButton.setOnAction(e -> { 
			if(fileSelect != null)
				modecontorl.sendMessageFile(fileSelect);
			else
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Select a File");

				alert.showAndWait();
			}
		});
		attacheFileButton.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Select a File to send");
			fileSelect = fileChooser.showOpenDialog(null);
			
			if(fileSelect != null)
			{
				sendFileButton.setDisable(false);
				Platform.runLater(() -> { ruteFileTextField.setText(fileSelect.getAbsolutePath()); });
			}
		});
		
	}

	public VBox getFileZone(double parentWidth)
	{
		VBox fileZone = new VBox(5);
		fileZone.setPadding(new Insets(AbsModePanel.MARGE_BETWEEN, 
										AbsModePanel.MARGE_RIGHT, 
										AbsModePanel.MARGEN_BOTTOM, 
										AbsModePanel.MARGE_LEFT));
		
		//Design:
		sendFileButton.setMinWidth(parentWidth - (AbsModePanel.MARGE_RIGHT + AbsModePanel.MARGE_LEFT));
		BorderPane attachFileSection = new BorderPane();
		attachFileSection.setCenter(ruteFileTextField);
		attachFileSection.setRight(attacheFileButton);
		fileZone.getChildren().addAll(attachFileSection,
									sendFileButton);
		
		return fileZone;		
	}

	/****************************************/
	/******* GUI Change LISTENER	*********/
	public void enableToSendFile(boolean stat)
	{
		Platform.runLater(() -> {
			attacheFileButton.setDisable(!stat);
		});
	}
	/******* (END) GUI Change LISTENER	*********/	
	/********************************************/

}
