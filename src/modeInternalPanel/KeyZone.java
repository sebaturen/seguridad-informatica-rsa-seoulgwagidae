package modeInternalPanel;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import panel.AbsModePanel;
import userControl.Mode;

/**
 * Key zone GUI:
 * _____________________________________________________________________
 * |																	|
 * |	------------------------------------------------------------	|
 * |	[															]	|
 * |	[	KEY LOG (TEXT AREA)										]	|
 * |	[															]	|
 * |	------------------------------------------------------------	|
 * |	{Generate Key (BUTTON)} {Load key from Storage (BUTTON)} {Save key in Storage (BUTTON)}
 * |	{Share a Public Key (BUTTON)} [LAST ENCRYPED KEY (TEXT AREA)]	|
 * |____________________________________________________________________|	
 * @author sturen
 *
 */
public class KeyZone {
	
	private TextArea keyLogTextArea  	= new TextArea();
	private TextArea pariedKeyTextArea	= new TextArea();
	private Button generateKeyButton 	= new Button("Generate Key");
	private Button loadKeyFileButton 	= new Button("Load key from Storage");
	private Button saveKeyFileButton 	= new Button("Save key in Storage");
	private Button shareKeyButton		= new Button("Share a Public Key");
	private static final int shareKeySize			= 150;
	
	public KeyZone(Mode modeControl)
	{
		//Constant Design:
		keyLogTextArea.setPrefRowCount(2);
		keyLogTextArea.setEditable(false);
		pariedKeyTextArea.setPrefRowCount(1);
		pariedKeyTextArea.setEditable(false);
		shareKeyButton.setMinWidth(shareKeySize);
		shareKeyButton.setDisable(true);

		//Action listener
		generateKeyButton.setOnAction(e -> { modeControl.generateKey(); });
		loadKeyFileButton.setOnAction(e -> { modeControl.loadKeyFromStorage(); });
		saveKeyFileButton.setOnAction(e -> { modeControl.saveKeyInStorage();});
		shareKeyButton.setOnAction	 (e -> { modeControl.sharePublicKey(); });
		
	}
	
	public BorderPane getKeyzone(double parentWidth)
	{
		BorderPane keyZone = new BorderPane();
		keyZone.setPadding(new Insets(AbsModePanel.MARGE_BETWEEN, 
										AbsModePanel.MARGE_RIGHT, 
										0, 
										AbsModePanel.MARGE_LEFT));
		
		//Top:
		VBox kyLog = new VBox();
		kyLog.setPadding(new Insets(0, 0, AbsModePanel.MARGE_BETWEEN, 0));
		kyLog.getChildren().add(keyLogTextArea);
		keyZone.setTop(kyLog);
		
		//Center:
		BorderPane buttonZone1 = new BorderPane();
		buttonZone1.setPadding(new Insets(0, 0, AbsModePanel.MARGE_BETWEEN, 0));
		buttonZone1.setLeft(generateKeyButton);
		buttonZone1.setCenter(loadKeyFileButton);
		buttonZone1.setRight(saveKeyFileButton);
		keyZone.setCenter(buttonZone1);
		
		//Bottom:
		HBox zoneBottom = new HBox(5);
		//zoneBottom.setPadding(new Insets(8, 12, 8, 12));
		pariedKeyTextArea.setMinWidth(parentWidth - (AbsModePanel.MARGE_LEFT + AbsModePanel.MARGE_RIGHT) - shareKeySize);
		zoneBottom.getChildren().addAll(shareKeyButton,
										pariedKeyTextArea);
		keyZone.setBottom(zoneBottom);
		
		return keyZone;
	}
	
	/****************************************/
	/******* GUI Change LISTENER	*********/
	public void addKeyLog(String log)
	{
		Platform.runLater(() -> { keyLogTextArea.appendText(log +"\n"); });
	}
	
	public void setPariedKey(String s)
	{
		Platform.runLater(() -> { pariedKeyTextArea.setText(s); });
	}
	
	public void enableShareKey(boolean stat)
	{
		Platform.runLater(() -> { shareKeyButton.setDisable(!stat); });
	}
	/******* (END) GUI Change LISTENER	*********/	
	/********************************************/
}
