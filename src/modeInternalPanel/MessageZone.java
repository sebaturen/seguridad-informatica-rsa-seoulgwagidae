package modeInternalPanel;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import panel.AbsModePanel;
import userControl.Mode;

/**
 * Message zone GUI:
 * _____________________________________________________
 * |													|
 * |	Last Encrypted Message: (LABEL)    				|
 * |	[	LAST ENCRYPTED MSG (TEXT AREA)			]	|
 * |	--------------------------------------------	|
 * |	[											]	|
 * |	[	SHOW MESSAGE AREA (TEXT AREA)			]	|
 * |	[											]	|
 * |	----------------------------------------------	|
 * |	[ WRITE MESSAGE (TEXT FIELD) ] {Send (BUTTON)}	|
 * |____________________________________________________|	
 * @author sturen
 *
 */
public class MessageZone {

	private TextArea lastEncryptMsgTextArea = new TextArea();
	private TextArea messagesTextArea		= new TextArea();
	private TextField writeMessageTextField	= new TextField();
	private Button sendButton				= new Button("Send");
	
	public MessageZone(Mode modeControl)
	{
		//Constant Design:
		lastEncryptMsgTextArea.setPrefRowCount(1);
		lastEncryptMsgTextArea.setEditable(false);
		messagesTextArea.setPrefRowCount(5);
		messagesTextArea.setEditable(false);
		enableToTypMsg(false);
		
		//Action listener:
		EventHandler<ActionEvent> sendMessageEvent = e -> {
			if (writeMessageTextField.getText().length() > 0)
			{
				modeControl.sendMessage(writeMessageTextField.getText());
				writeMessageTextField.setText("");
			}
		}; 
		sendButton.setOnAction(sendMessageEvent);
		writeMessageTextField.setOnAction(sendMessageEvent);
	}
	
	public VBox getMessageZone()
	{
		VBox messageZone = new VBox(5);
		messageZone.setPadding(new Insets(AbsModePanel.MARGE_BETWEEN, 
											AbsModePanel.MARGE_RIGHT, 
											0, 
											AbsModePanel.MARGE_LEFT));
						
		//Send message section [input text] (Send button)
		BorderPane sendMessageSection = new BorderPane();
		sendMessageSection.setCenter(writeMessageTextField);
		sendMessageSection.setRight(sendButton);
		
		messageZone.getChildren().addAll(new Label("Last Encrypted Message:"),
									lastEncryptMsgTextArea,
									messagesTextArea,
									sendMessageSection);
		
		return messageZone;
	}

	/****************************************/
	/******* GUI Change LISTENER	*********/
	public void addMessaje(String s)
	{
		Platform.runLater(() -> { messagesTextArea.appendText(s +"\n"); });
	}
	
	public void setLastEncriptMsg(String s)
	{
		Platform.runLater(() -> { lastEncryptMsgTextArea.setText(s); });
	}
	
	public void enableToTypMsg(boolean stat)
	{
		Platform.runLater(() -> {
			sendButton.setDisable(!stat);
			writeMessageTextField.setDisable(!stat);
		});
	}
	/******* (END) GUI Change LISTENER	*********/	
	/********************************************/
}
