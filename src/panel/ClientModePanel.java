package panel;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import modeInternalPanel.ConnectionZone;
import userControl.ClientMode;

public class ClientModePanel extends AbsModePanel {

	private TextField serverIpTextField 	= new TextField();
	private TextField serverPortTextField	= new TextField();
	private Button connectButton			= new Button("Connect");
	
	//Constructor
	public ClientModePanel(ClientMode mod)
	{
		super(mod);
	}
	  
	/**
	 * Define a new status Connect / Disconnect
	 * @param stat
	 */
	public void setSetatusConect(boolean stat)
	{
		Platform.runLater(() -> { 
			if(stat)
				connectButton.setText("Disconnect");
			else
				connectButton.setText("Connect");
			serverIpTextField.setDisable(stat);
			serverPortTextField.setDisable(stat); 
		});
	}
	
	/**
	 * Action to connect button is trigger
	 */
	private void connectButton()
	{
		if(!ClientMode.getStatusConnection())
		{
			try {
				((ClientMode) super.modeControl).
					generateConnection(serverIpTextField.getText(), 
										Integer.parseInt(serverPortTextField.getText()));
			} catch (NumberFormatException e) {
				super.addLog("Set a valid IP and Port");
			}
		}
		else
		{
			super.modeControl.closeConnection();
		}
	}
	
	/**
	 * This call a ConnectionZone class, to get a connection panel and add
	 * a specific client information.
	 */
	protected ConnectionZone connectPanel(double parentWidth)
	{
		VBox connectInfoZone = new VBox(5);
		connectInfoZone.setPrefWidth(parentWidth/4 - 12); //Divide parent width in 4 and rest border
		//serverPortTextField.setPrefWidth(10);
		connectInfoZone.getChildren().addAll(new Label("Server IP:"),
											serverIpTextField,
											new Label("Server Port:"),
											serverPortTextField,
											connectButton);
		ConnectionZone clientZone = new ConnectionZone(connectInfoZone);
		
		//Action listener:
		connectButton.setOnAction(e -> { connectButton(); });
		
		return clientZone;
	}
}
