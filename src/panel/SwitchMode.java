package panel;

import java.io.File;

import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rsa.ConnectInformation;
import rsa.RSA;

public class SwitchMode extends BorderPane {

    public static final String[] runModeList = {"Client", "Server"};
    private String[] lastUserIdentList = {"<new user>"};
    private ComboBox<String> runModeComboBox;
    private ComboBox<String> lastUserComboBox;
    private TextField userNameTextField;
    private Stage fatherStage;
    
    public SwitchMode(Stage fatherStage)
    {
    	this.fatherStage = fatherStage;
		lastUserLoad();
    }
	
    /**
     * Load a last user, use a "clientFile" try to load last information
     * if exist a folder, we load a key in login time.
     */
    private void lastUserLoad()
    {
        File f = new File(ConnectInformation.CLIENT_FILE_PATH);
        if(f.exists())
        {            
            File[] lisF = f.listFiles();
            lastUserIdentList = new String[ lisF.length + 1 ]; //All list + 1 -> new user
            lastUserIdentList[0] = "<new user>";
            for(int i = 0; i < lisF.length; i++)
            {          
                lastUserIdentList[i+1] = lisF[i].getName();
            }
        }
    }
	
	/**
	 * Run information (Client, server, user name) 
	 */
	private VBox getRunInformation()
	{
		VBox info = new VBox(10);
	    info.setPadding(new Insets(14, 12, 0, 12));
	    info.setFillWidth(true);
		info.getChildren().add(new Label("Select a functionality mode:"));
		
		//Run mode
		runModeComboBox = new ComboBox<>();
		info.getChildren().add(runModeComboBox);
		runModeComboBox.setPrefWidth(getWidth());
	    runModeComboBox.getItems().addAll(runModeList);
	    runModeComboBox.setValue(runModeList[0]);
	    runModeComboBox.setOnAction(e -> {
	    	boolean hideUserInfo = false;
	    	if( (runModeComboBox.getValue()).equals(runModeList[1]) ) hideUserInfo = true;
	    	lastUserComboBox.setDisable(hideUserInfo);
	    	userNameTextField.setDisable(hideUserInfo);
	    });
	    
		//Last user list
	    lastUserComboBox = new ComboBox<>();
	    userNameTextField = new TextField();
		info.getChildren().add(lastUserComboBox);
		lastUserComboBox.setPrefWidth(getWidth());
		info.getChildren().add(userNameTextField);
		lastUserComboBox.getItems().addAll(lastUserIdentList);
		lastUserComboBox.setValue(lastUserIdentList[0]);
		lastUserComboBox.setOnAction(e -> {
			boolean disableNewName = true;
	    	if( (lastUserComboBox.getValue()).equals(lastUserIdentList[0]) ) disableNewName = false;
	    	userNameTextField.setDisable(disableNewName);			
		});
		
		return info;
	}

	/**
	 * Button RUN / EXIT
	 */
	private BorderPane getButtonOption()
	{
		Button runButton = new Button("Run");
		Button exitButton = new Button("Exit");
		runButton.setOnAction(e -> { runButton(); });
		exitButton.setOnAction(e -> { System.exit(0); });
		
		BorderPane button = new BorderPane();
		button.setPadding(new Insets(0, 12, 14, 12));
		button.setLeft(runButton);
		runButton.setPrefWidth(70);
		button.setRight(exitButton);
		exitButton.setPrefWidth(70);
		
		return button;		
	}

	/**
	 * Run button action
	 */
	private void runButton()
	{
		if( (runModeComboBox.getValue()).equals(runModeList[0]) &&
			(lastUserComboBox.getValue()).equals(lastUserIdentList[0])	&&
			(userNameTextField.getText()).length() == 0)
		{ //ERROR!, Client mode and no put any username
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Put a username");

			alert.showAndWait();
		}
		else
		{
			String userName = "Server";
			byte runMode = ConnectInformation.SERVER_MODE;
			if((runModeComboBox.getValue()).equals(runModeList[0])) //Client mode
			{
				runMode = ConnectInformation.CLIENT_MODE;
				userName = lastUserComboBox.getValue(); //Previews user
				if(userName.equals(lastUserIdentList[0])) //New User
				{
					userName = userNameTextField.getText();					
				}
			}
			RSA.runMode = runMode;
			RSA.userName = userName;
			fatherStage.hide();
		}
	}

	/** PAINT ZONE!: **/
	public void paint()
	{
		//Load a last user list.
		this.setCenter(getRunInformation());
		this.setBottom(getButtonOption());
	}
	
	@Override
	public void setWidth(double width) {
		super.setWidth(width);
		paint();
	}

	@Override
	public void setHeight(double height) {
		super.setHeight(height);
		paint();
	}
}
