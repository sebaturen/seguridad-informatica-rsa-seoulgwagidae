package panel;

import javafx.scene.layout.VBox;
import modeInternalPanel.ConnectionZone;
import modeInternalPanel.FileZone;
import modeInternalPanel.KeyZone;
import modeInternalPanel.MessageZone;
import userControl.Mode;

/**
 * This class is a complete panel control.
 * the GUI is divide en 4 zone:
 * ______________________________
 * |							|
 * |	(1)	Connection Zone    	|
 * |____________________________|
 * |							|
 * |	(2)	Key Zone			|
 * |____________________________|
 * |							|
 * |	(3)	Message Zone		|
 * |____________________________|
 * |							|
 * |	(4)	File Zone			|
 * |____________________________|
 * 
 *  and all zone is divide in different class, all class return a panel.
 *  
 * @author sturen
 *
 */
public abstract class AbsModePanel extends VBox {
	
	//Constant
	public static final byte MARGE_LEFT 	= 12;
	public static final byte MARGE_RIGHT	= 12;
	public static final byte MARGE_BETWEEN	= 5;
	public static final byte MARGEN_TOP		= 8;
	public static final byte MARGEN_BOTTOM	= 8;
	
	//Variable
	private ConnectionZone connectPanel;
	private KeyZone keyZone;
	private MessageZone messageZone;
	private FileZone fileZone;
	protected Mode modeControl;
	
	public AbsModePanel(Mode modeControl)
	{
		this.modeControl = modeControl;
		keyZone = new KeyZone(modeControl);
		messageZone = new MessageZone(modeControl);
		fileZone = new FileZone(modeControl);
	}

    /**************************************************/
    /******************LOG Methods*********************/
	public void addLog(String s)
	{
		connectPanel.addLog(s);
	}
	
	public void setServerConect(String s)
	{
		connectPanel.setServerConect("Connection Status: "+ s);
	}
	
	public void addKeyLog(String log)
	{
		keyZone.addKeyLog(log);
	}
	
	public void setPariedKey(String s)
	{
		keyZone.setPariedKey(s);
	}
	
	public void enableShareKey(boolean stat)
	{
		keyZone.enableShareKey(stat);
	}
	
	public void addMessaje(String s)
	{
		messageZone.addMessaje(s);
	}
	
	public void setLastEncriptMsg(String s)
	{
		messageZone.setLastEncriptMsg(s);
	}
	
	public void enableToTypMsg(boolean stat)
	{
		messageZone.enableToTypMsg(stat);
		fileZone.enableToSendFile(stat);
	}
    /****************END LOG Methods*******************/
    /**************************************************/
	
		
	/** PAINT ZONE!: **/
	public void paint()
	{
		//Clear
		this.getChildren().clear();
		//Add zones:
		connectPanel = connectPanel(super.getWidth());
		this.getChildren().add(connectPanel.getConnectionZone(super.getWidth()));
		this.getChildren().add(keyZone.getKeyzone(super.getWidth()));
		this.getChildren().add(messageZone.getMessageZone());
		this.getChildren().add(fileZone.getFileZone(super.getWidth()));
	}
		
	@Override
	public void setWidth(double width) {
		super.setWidth(width);
		paint();
	}

	@Override
	public void setHeight(double height) {
		super.setHeight(height);
		paint();
	}
	
	
	/** ABSTRACT METHOD **/
	protected abstract ConnectionZone connectPanel(double parentWidth);
}
