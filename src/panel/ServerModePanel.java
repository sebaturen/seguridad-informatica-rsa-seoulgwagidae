package panel;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.application.Platform;
import modeInternalPanel.ConnectionZone;
import userControl.ServerMode;

public class ServerModePanel extends AbsModePanel {

	private TextField portTextField			= new TextField();
	private Button switchPortButton 		= new Button("Switch Port");
	private int lastPort					= 0;
	
	public ServerModePanel(ServerMode mod)
	{
		super(mod);
	}

	/**
	 * Port listener change GUI
	 * @param portList
	 */
	public void setListtenerPort(int portList)
	{
		lastPort = portList;
		Platform.runLater(() -> { portTextField.setText(portList +""); });
	}
	
	/**
	 * When switch port is trigger.
	 */
	private void switchPort()
	{
		try {
			int port = Integer.parseInt(portTextField.getText());
			if (lastPort == port)
				super.addLog("Use a different port");
			else				
				((ServerMode) super.modeControl).setListennerPort(port);
		} catch (NumberFormatException e) {
			super.addLog("The port only accepts numbers");
		}
	}
	
	/**
	 * This call a ConnectionZone class, to get a connection panel and add
	 * a specific server information.
	 */
	protected ConnectionZone connectPanel(double parentWidth)
	{
		VBox connectInfoZone = new VBox(5);
		connectInfoZone.setPrefWidth(parentWidth/4 - 12); //Divide parent width in 4 and rest border
		connectInfoZone.getChildren().addAll(new Label("Listener Port:"),
											portTextField,
											switchPortButton);
		ConnectionZone serverZone = new ConnectionZone(connectInfoZone);
		
		//Action listener
		switchPortButton.setOnAction(e -> { switchPort(); });
		
		return serverZone;
	}
}
