/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControl;

import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import javafx.scene.Parent;
import panel.ClientModePanel;

/**
 * Control Client mode, connect to server and interface
 * @author Seba
 */
public final class ClientMode extends Mode {
    
    //Variable
    private int conectPort;
    private String conectIP;
    private Thread runClientThread;
    //Visual Panel
    private ClientModePanel clientModePanel;
    private static boolean statusConnection = false;
    
    public ClientMode(String ident, byte runMode) 
    {
    	super(ident, runMode);
    	clientModePanel = new ClientModePanel(this);
    	super.modePanel = clientModePanel;
        log_lastKeyLog(); //Internal control, get a log previews key generate~
    }   
    
    /**
     * Set a run client~
     */
    public void runCliente()
    {
    	//Run in new thread~
        runClientThread = new Thread(() ->
        {
            //Try to connect to server.
            try
            {
                connectToSever(); //Generate connection
                setupStreams(); //Open streams
                changeStatusConnect(true);
                //send my name to server, he can load key if he have.
                sendMessage( (getIdent()).getBytes(), false, HI_INFORMATION_TRANSMISION);
                whileChatting(); //sending and receive message
            } 
            catch (EOFException e)
            {
            	clientModePanel.addLog("Conection is close");
            }
            catch (IOException e)
            {
            	clientModePanel.addLog("Disconnected");
            } 
            catch (IllegalArgumentException e)
            {
            	clientModePanel.addLog("Fail to connect server, incorrect data");
            }
            finally 
            {
                closeConnection();
            }
            changeStatusConnect(false);
        });
        runClientThread.start();
    }
    
    /**
     * Connect to server
     * @throws IOException
     */
    private void connectToSever() throws IOException
    {
    	clientModePanel.setServerConect("Attempting connection...");
        socket = new Socket(conectIP, conectPort);
    }
    
    /**
     * Interface button generate connection, set IP and port
     * @param ip
     * @param port
     */
    public void generateConnection(String ip, int port)
    {        
        this.conectIP = ip;
        this.conectPort = port;
        if(socket != null)
            closeConnection(); //Close last possible connection
        runCliente();
    }

    /**
     * Close a server connection
     */
    @Override
    public void closeConnection() {
        closeConnectionFinal();
        changeStatusConnect(false);
    }
    
    public static boolean getStatusConnection()
    {
    	return statusConnection;
    }
    
    /**
     * Set a visual parameter
     */
    public Parent getPanel()
    {
    	return clientModePanel;
    }
    
    /**************************************************/
    /*****************Visual Method********************/
    private void changeStatusConnect(boolean stat)
    {
    	if (stat)
    		clientModePanel.setServerConect("Connected to "+ socket.getInetAddress().getHostName());
    	statusConnection = stat;
    	clientModePanel.setSetatusConect(stat);
    }
}
