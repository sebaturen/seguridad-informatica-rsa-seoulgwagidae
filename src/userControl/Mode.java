/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javafx.scene.Parent;
import panel.AbsModePanel;
import rsa.ConnectInformation;
import rsa.EncryptionController;

/**
 * Abstract class, implemented all commune function to Client mode and Server mode
 * Control send data, Cryptography object, etc.
 * @author Seba
 */
public abstract class Mode implements ConnectInformation {
	
	private static String ident;
    private static byte runMode;
    
    //Attribute - Socket communication
    private ObjectOutputStream output;
    private ObjectInputStream input;
    protected Socket socket;
    //Attribute - message
    private EncryptionController encryptControl;
    private String lastMsg;
    protected AbsModePanel modePanel;
    
    private boolean isSharePubliKey;
    protected Thread runThread;
    private static String opostName = "NOT - CONNECTED";
    
    protected Mode(String sIdent, byte sRunMode)
    {
    	ident = sIdent;
    	runMode = sRunMode;
    	System.out.println(ident +" is running");
        //Generate and load RSA Key:
        //RSA Control load
        encryptControl = new EncryptionController();   
    }
    
    /**
     * get Stream to send and receive different data
     * @throws IOException
     */
    protected void setupStreams() throws IOException
    {
        output = new ObjectOutputStream(socket.getOutputStream());
        output.flush(); //clear... if need send is ready
        input = new ObjectInputStream(socket.getInputStream());
        log_addLog("Streams is setting");
        //Enable button share key:
        enableShareKey(true);
    }
    
    /**
     * message exchange
     * @throws IOException
     */
    protected void whileChatting() throws IOException
    {
        //Read input date
        do {
            try
            {
                //Get message Object
                byte[] inputData = (byte[]) input.readObject();
                //get a type of message
                byte nextMsgType = inputData[0];
                
                //Copy a data in new byte array
                byte[] data = new byte[inputData.length - 1];
                System.arraycopy(inputData, 1, data, 0, inputData.length - 1);
                
                //Switch different type
                switch(nextMsgType)
                {
	                case HI_INFORMATION_TRANSMISION:
	                    readHiInformation(data);                     
	                    break;
                    case PUBLY_KEY_TRANSMISION:
                        readPubliKeyParied(data);
                        break;
                    case STRING_MSG_TRANSMISION:
                        readStringMsgEncripted(data);
                        break;
                    case FILE_TRANSMISION:
                        readFileMsgEncripted(data);
                        break; 
                    case SYMETRIC_KEY_TRANSMISION:
                        readSymetricKeyParied(data);
                        break;
                    case FAIL_DECRYPT_MSG_NOTIFICATION:
                    	log_addMessaje("We receive the message, but could not be decrypted");
                    	break;
                        
                }
            }
            catch (ClassNotFoundException e)
            {
                log_addLog("Message is NOT recognized");
            }
        } while(true && socket.isConnected());
    }
        
    /**
     * Close connection Cliente-server
     */
    protected void closeConnectionFinal()
    {
        //Disable visual elements
        enableTypMsg(false);
        enableShareKey(false);
        log_addConnectStatus("NO - CONNECTED");
        //Clear paired key
        encryptControl.clearConectPublicKey();
        isSharePubliKey = false;
        //Clear socket RED connection
        try
        {
            if (output != null)
                output.close();
            if (input != null)
                input.close();
            if (socket != null)
                socket.close();
            if (runThread != null)
                runThread.interrupt();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /* ============================================== */
    /*           READ MESSAGE  - section              */
    /* ============================================== */
    /**
     * Client or Server identity information, and try load a
     * public key if we know who is
     * @param msg
     */
    private void readHiInformation(byte[] msg)
    {
        try {
            //Register actual connection
            opostName = new String(msg);
            //Try load a connecter public key
            encryptControl.loadPariedPubliKeyFile( opostName );
            log_addLog(EncryptionController.getLog());
            enableTypMsg(true);
            log_setPariedKey(encryptControl.getStringConnectPubliKey());
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException ex) {
            log_addKeyLog("There is no public KEY for "+ opostName);
        }
    }
    
    /**
     * Get a encrypt message and decrypt
     * when have a message, show in visual interface
     * @param msg
     */
    private void readStringMsgEncripted(byte[] msg)
    {
        //READ AND ENCRIPTD MSG!
        try {
            //Visual encrypt message show:
            log_addEncriptMsg(new String( msg ) );
            //decrypt message
            byte[] descMsg = encryptControl.desencriptMsg( msg );
            String msgFinal = new String(descMsg);
            log_addMessaje(opostName +": "+ msgFinal);
        } catch (NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException ex) {
            log_addKeyLog("FAILED decrypt message");
            log_addMessaje(opostName +": MESSAGE RECEIVED, CAN NOT BE DECRYPT");
            //notification other client - server we can't decrypt msg:
            sendMessage(new byte[0], false, FAIL_DECRYPT_MSG_NOTIFICATION);
        }
    }
    
	/**
	 * The client-server send a public key, this method read
	 * and save in encrypt controller
	 * @param msg
	 */
	private void readPubliKeyParied(byte[] msg)
	{
	    try {
	        //Receive a public key
	        encryptControl.setConnectPublicKey( msg );
	        log_addKeyLog(EncryptionController.getLog());
	        log_setPariedKey(encryptControl.getStringConnectPubliKey());
	        //Enable user typing
	        enableTypMsg(true);
	        //Send client public key
	        if(encryptControl.isKey() && !isSharePubliKey)
	        {
	            sendMessage( encryptControl.getPublicKey(), false, PUBLY_KEY_TRANSMISION );
	            isSharePubliKey = true;                            
	        }                        
	    } catch (NoSuchAlgorithmException | InvalidKeySpecException | ClassCastException | IOException ex) {
	        log_addKeyLog("FAILED save paried key");
	    }
	}
    
    /**
     * The client-server send a symmetric key, this method
     * save in encrypt controller
     * @param msg
     */
    private void readSymetricKeyParied(byte[] msg)
    {
        try {
            //Decrypt message
            byte[] descMsg = encryptControl.desencriptMsg( msg );
            encryptControl.setSymmetricKey( descMsg );
            log_addKeyLog(EncryptionController.getLog());
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            log_addKeyLog("FAILED paried Symmetric key: "+ ex);
        }
    }
    
    /**
     * Receive a file, decrypt and validate the signature
     * if all is okey, save in a file directory. 
     * @param msg
     */
    private void readFileMsgEncripted(byte[] msg)
    {
        if(!encryptControl.isSymmetricKey())
            log_addKeyLog("Not have a symmetric key, can't desencrypt Fail");
        else
        {
            try {
                //Decrypt file:
                byte[] fByte = encryptControl.desencriptSymmetricMsg( msg);
                
                //Separate signature, name file and file Date
                //Data is like: byte[] = [SIGNATURE][(1 BYTE)SIZE NAME][NAME][DATE FILE]
                byte[] signature = new byte[EncryptionController.KEY_SIZE / 8];
                byte[] nameFileByte = new byte[ fByte[EncryptionController.KEY_SIZE / 8] ];
                byte[] fileData = new byte[ fByte.length - ( (EncryptionController.KEY_SIZE / 8) + 1 + nameFileByte.length) ];
                
                //Copy signature
                System.arraycopy(fByte, 0, signature, 0, signature.length);
                int postCopy = signature.length +1;
                //Copy name
                System.arraycopy(fByte, postCopy, nameFileByte, 0, nameFileByte.length);
                postCopy += nameFileByte.length;
                //Copy file data
                System.arraycopy(fByte, postCopy, fileData, 0, fileData.length);
                
                //Validate integrity (signature validate)
                if(!encryptControl.validateSignature(fileData, signature))
                    log_addKeyLog("Signature FAIL match");
                else
                {
                    log_addKeyLog("Signature IS match");
                    
                    //Prepare directory:
                    String nameFile = new String(nameFileByte);
                    String filePath = getFileLocation() + TRANSMITION_FILE_PATH + nameFile;
                    File fileRecive = new File(filePath.replaceAll("(\\w+)_(\\d+).*", "$1/$2/$0"));
                    fileRecive.getParentFile().mkdirs();
                    fileRecive.createNewFile();
                    
                    //Save file
                    try (FileOutputStream fos = new FileOutputStream(fileRecive, false)) {
                        fos.write(fileData);
                    } catch (IOException ex) {
                        Logger.getLogger(Mode.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    //log
                    log_addMessaje("A new file arrived, save in: "+ filePath);
                    log_addLog("SUCCERFULLY saved FILE");
                    
                    //Destroy the temporary name
                    nameFile = null;
                }
            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
                log_addKeyLog("FAILED desecript message");
            } catch (SignatureException ex) {
                log_addKeyLog("FAILED signature");
            } catch (IOException ex) {
                log_addLog("FAILED save file: "+ ex);
            }
        }
    }
    
    /* ============================================== */
    /*           SEND MESSAGE  - section              */
    /* ============================================== */
    /**
     * Send a message to client-server.
     * Get message, encrypted and send IF NOT HAVE A PUBLIC KEY FOR DESTINY DON'T CANT!
     * @param msg
     * @param typeMsg
     */
    public void sendMessage(String msg)
    {
        if(encryptControl.isConnectPubliKey())
        {
            try {
                lastMsg = msg;
                byte[] encriptMsg = encryptControl.encriptMsg(msg);
                sendMessage( encriptMsg , true, STRING_MSG_TRANSMISION);
            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
                log_addLog("FAILED message encryption: "+ msg);
            }             
        }   
        else
        {
            log_addLog("NOT HAVE A PUBLIC KEY FROM DESTINY");
        } 
    }
 
	/**
	 * Send message to client-server * Byte!
	 * this function NOT ENCRYPTED message!
	 * @param date 		[content message]
	 * @param showMsg 	[is show in message text area?]
	 * @param typeMsg 	[type of message]
	 */
	protected void sendMessage(byte[] data, boolean showMsg, byte typeMsg)
	{
	    try
	    {
	    	//Add in head, the type of message:
	    	//New data send: [type][content]
	    	byte[] sendData = new byte[data.length + 1];
	    	sendData[0] = typeMsg;
	    	System.arraycopy(data, 0, sendData, 1, data.length);
	        //Send message:
	        output.writeObject(sendData);
	        output.flush();
	        if(showMsg)
	            log_addMessaje("Me: "+ lastMsg);
	    }
	    catch (IOException e)
	    {
	        log_addLog("FAILED send MESSAGE");
	    }        
	}
      
    /**
     * Encrypted and send file, create a signature and symmetric key
     * @param file
     */
    public void sendMessageFile(File file)
    {
        if(!file.exists())
            log_addLog("File not found");
        else
        {
            //Prepared symmetric key:
            shareSymmetricKey();
            
            //if symmetric key is okey
            if(encryptControl.isSymmetricKey())
            {          
                try {
                    //Prepared file transmission
                    byte[] fileByte = new byte[(int) file.length()];
                    try (FileInputStream fileInputStream = new FileInputStream(file)) {
                        fileInputStream.read(fileByte);
                    }
                    byte[] nameFile = (file.getName()).getBytes();
                    //Prepared signature:
                    byte[] signature = encryptControl.createSignature(fileByte);
                    
                    //Create a transfer information: byte[] = [SIGNATURE][(1 BYTE)SIZE NAME][NAME][DATE FILE]
                    byte[] fileSignatureAndFileNameAndData = new byte[signature.length + 1 + nameFile.length + fileByte.length];
                    //Copy signature:
                    System.arraycopy(signature, 0, fileSignatureAndFileNameAndData, 0, signature.length);
                    //Name size:
                    fileSignatureAndFileNameAndData[signature.length] = (byte) nameFile.length;
                    int postCopy = signature.length + 1;
                    //Copy name:                   
                    System.arraycopy(nameFile, 0, fileSignatureAndFileNameAndData, postCopy, nameFile.length);
                    postCopy += nameFile.length;
                    //Copy data:
                    System.arraycopy(fileByte, 0, fileSignatureAndFileNameAndData, postCopy, fileByte.length);
                                        
                    //Encrypt use a symmetric method
                    byte[] encriptData = encryptControl.encriptSymmetricMsg(fileSignatureAndFileNameAndData);
                                        
                    //Send a file
                    sendMessage(encriptData, false, FILE_TRANSMISION);
                    
                    //Log
                    log_addLog("File sucerfull sended...");
                } catch (FileNotFoundException ex) {
                    log_addLog("File not found");
                } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException | SignatureException ex) {
                    log_addLog("FAILED encript Symmetric file: "+ ex);
                }
            }         
        }
    }
    
    /* ============================================== */
    /*            Key Control  - section              */
    /* ============================================== */
    /**
     * Generate a Public-Private key RSA method
     */
    public void generateKey()
    {
        try {
            encryptControl.generateKey();
            log_addKeyLog(EncryptionController.getLog());
        } catch (NoSuchAlgorithmException | IOException ex) {
            log_addKeyLog("FAILED generate KEY");
        }
    }
    
    /**
     * Try to load the key from a file
     */
    public void loadKeyFromStorage()
    {
        try {
            encryptControl.loadKey();
            log_addKeyLog(EncryptionController.getLog());
        } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException ex) {
            log_addKeyLog("FAILED load KEY from FILE");
        }
    }
    
    /**
     * Try save the public and private key in file
     */
    public void saveKeyInStorage()
    {
        if(encryptControl.isKey())
        {
            try {
                encryptControl.saveKey();
                log_addKeyLog(EncryptionController.getLog());
            } catch (IOException ex) {
                log_addKeyLog("FAILED SAVE KEY from FILE");
            }            
        }
        else
            log_addKeyLog("KEY not yet created");
    }
    
    /**
     * Share public key client-server / server-client
     */
    public void sharePublicKey()
    {
        if(encryptControl.isKey())
        {
            sendMessage( encryptControl.getPublicKey(), false, PUBLY_KEY_TRANSMISION ); 
            isSharePubliKey = true;            
        }
        else
            log_addKeyLog("A KEY has not yet been LOADED");
    }
  
    /**
     * Share symmetric key client-server / servidor-cliente
     */
    private void shareSymmetricKey()
    {
        try {
            encryptControl.generateSymmetricKey(); 
            log_addKeyLog(EncryptionController.getLog());
            
            //Encrypted symmetric key. Use a RSA (public key)
            byte[] encriptKey = encryptControl.encriptMsg( encryptControl.getSymmetricKey() );
            
            //Send a symmetric key
            sendMessage( encriptKey , false, SYMETRIC_KEY_TRANSMISION );
            log_addKeyLog("Symmetric KEY was shared");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            log_addKeyLog("FAILED send a symetric KEY");
        }
    }
    
    /* ============================================== */
    /*    GET and SET  - section      */
    /* ============================================== */ 
    /**
     * Get a client or server name (who generate a opposite connection)
     * @return
     */
    public static String getOpostName()
    {
        return opostName;
    }
    
    public static String getIdent()
    {
    	return ident;
    }    
    
    /**
     * Get a file location
     * @return
     */
    public static String getFileLocation()
    {
        if(runMode == SERVER_MODE)
        {
            return SERVER_FILE_PATH;
        }
        else //client mode
        {      
            return CLIENT_FILE_PATH + ident +"/";
        }
    }

    /* ============================================== */
    /*       Visual change function  - section        */
    /* ============================================== */  
    protected void log_lastKeyLog() {
    	log_addKeyLog(EncryptionController.getLog());
    }
    
    private void log_addKeyLog(String log) {
    	modePanel.addKeyLog(log);
    }
    
    private void log_addLog(String log) {
    	modePanel.addLog(log);
    }
	
    private void log_addMessaje(String s) {
    	modePanel.addMessaje(s);  	
    }
	
    private void log_setPariedKey(String key) {
    	modePanel.setPariedKey(key);
    }
	
    private void log_addEncriptMsg(String s) {
    	modePanel.setLastEncriptMsg(s);
    }
	
    private void log_addConnectStatus(String s) {
    	modePanel.setServerConect(s);
    }
	
    private void enableTypMsg(boolean v) {
    	modePanel.enableToTypMsg(v);
    }
	
    private void enableShareKey(boolean v) {
    	modePanel.enableShareKey(v);
    }
    
    
    /* ============================================== */
    /*      	Abstract function  - section          */
    /* ============================================== */  
    public abstract void closeConnection();
    
    public abstract Parent getPanel();
}
