/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControl;

import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;

import javafx.scene.Parent;
import panel.ServerModePanel;

/**
 * Control Server mode, wait a Client connect
 * @author Seba
 */
public final class ServerMode extends Mode {
    
    //Variable
    private int portListenner = 29170 + (int)(Math.random() * 829);
    //Visual Panel
    private ServerModePanel serverModePanel;

   
    public ServerMode(String ident, byte runMode) 
    {
    	super(ident, runMode);   
    	serverModePanel = new ServerModePanel(this); 
    	super.modePanel = serverModePanel;
        //runServerModeVisual();
    }
        
    /**
     * Set a visual parameter jFrame
     */
    public Parent getPanel()
    {
    	return serverModePanel;
    }
        
    /**
     * Generate a internal controls
     */
    private void runServerModeInternalControl()
    {        
        //Set a visual port listener
        serverModePanel.setListtenerPort(portListenner); 
        //Set a log
        log_lastKeyLog();  
    }
    
    /**
     * Run Server
     */
    public void runServer()
    {
        runServerModeInternalControl();
        runThread = new Thread(() -> 
        {
        	int runPortSocket = portListenner;
            try 
            {
                //Open Socket
            	ServerSocket serverSocket = new ServerSocket(runPortSocket);
                //wait infinite time...
                while(true)
                {
                    try 
                    {
                    	serverModePanel.addLog("Listening port: "+ runPortSocket);
                        waitForConnection(serverSocket); //wait a client
                        setupStreams(); //output and input stream
                        //send my name to client, he can load key if he have.
                        sendMessage( (getIdent()).getBytes(), false, HI_INFORMATION_TRANSMISION);
                        whileChatting(); //sending and receive message
                    }
                    catch (EOFException e)
                    {
                    	serverModePanel.addLog("Server END connection!");
                    }
                    catch (IOException e)
                    {
                    	serverModePanel.addLog("Client is log-out");
                    }
                    finally
                    {
                    	closeConnection();
                    }
                }
            }
            catch (IllegalArgumentException e)
            {
            	serverModePanel.addLog("Fail to listener port, out of range");
            }
            catch (IOException e)
            {
            	serverModePanel.addLog("Connection is cloused: "+ runPortSocket +" e: "+ e);
            }
        });
        runThread.start();
    }
    
    /**
     * wait for connection
     * @throws IOException
     */
    private void waitForConnection(ServerSocket sockServer) throws IOException
    {
    	serverModePanel.setServerConect("Waiting connection");
        socket = sockServer.accept();
        serverModePanel.setServerConect("Connection to "+ socket.getInetAddress().getHostName());
    }
    
    /**
     * Change listener port:
     * @param newPort
     */
    public void setListennerPort(int newPort)
    {
        this.portListenner = newPort;
        closeConnection();
        runServer();
    }

    /**
     * Close a Client connection
     */
    @Override
    public void closeConnection() {
    	closeConnectionFinal();
    }
}
